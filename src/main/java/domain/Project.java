package domain;

import java.util.ArrayList;
import java.util.List;

import factory.Board;
import factory.CreateBoardFactory;
import observer.Observable;
import observer.Observer;
import state.CodeReviewState;
import state.DoneState;
import state.InProgressState;
import state.TaskState;
import state.TestState;
import state.ToDoState;
import strategy.PipelineStrategy;

public class Project implements Observable {
    String name;
    List<Board> boards;
    List<User> users;
    Backlog backlog;
    public boolean isDone;
    CreateBoardFactory createBoardFactory;

    TaskState state;
    public ToDoState toDoState;
    public InProgressState inProgressState;
    public TestState testState;
    public CodeReviewState codeReviewState;
    public DoneState doneState;

    List<Observer> observers;

    public PipelineStrategy pipelineStrategy;

    public Project(String name, CreateBoardFactory createBoardFactory, PipelineStrategy pipelineStrategy) {
        this.name = name;
        this.boards = new ArrayList<Board>();
        this.users = new ArrayList<User>();
        backlog = new Backlog();
        this.isDone = false;
        this.createBoardFactory = createBoardFactory;
        this.state = new ToDoState(this);
        this.toDoState = new ToDoState(this);
        this.inProgressState = new InProgressState(this);
        this.testState = new TestState(this);
        this.codeReviewState = new CodeReviewState(this);
        this.doneState = new DoneState(this);
        this.observers = new ArrayList<Observer>();
        this.pipelineStrategy = pipelineStrategy;
    }

    public Backlog returnBacklog() {
        return backlog;
    }

    public void addBoard(String name, BoardType type) {
        if (type == BoardType.SCRUM) {
            Board scrum = this.createBoardFactory.createBoard(name, type);
            this.boards.add(scrum);
        } else if (type == BoardType.KANBAN) {
            Board kanban = this.createBoardFactory.createBoard(name, type);
            this.boards.add(kanban);
        }
    }

    public List<User> getUsers() {
        return users;
    }

    public Board getBoardByName(String name) {
        for (Board board : boards) {
            if (board.getBoardName().equals(name)) {
                return board;
            }
        }
        return null;
    }

    public void addUserToBoard(Board board, User user) {
        board.getUserList().add(user);
    }

    public void addBacklogItemToBoard(Board board, Task task) {
        board.getTaskList().add(task);
    }

    public void setState(TaskState state) {
        this.state = state;
    }

    public TaskState getToDoState() {
        return this.toDoState;
    }

    public TaskState getInProgressState() {
        return this.inProgressState;
    }

    public TaskState getTestState() {
        return this.testState;
    }

    public TaskState getCodeReviewState() {
        return this.codeReviewState;
    }

    public TaskState getDoneState() {
        return this.doneState;
    }

    public TaskState getState() {
        return this.state;
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        if(this.isDone == true) {
            for(Observer observer : this.observers) {
                observer.update(this.doneState);
            }
        }
    }

    public String runPipeline() {
        return this.pipelineStrategy.run();
    }
}
