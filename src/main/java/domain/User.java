package domain;

public class User {
    String name;
    String username;
    String email;

    public User(String name, String username, String email) {
        this.name = name;
        this.username = username;
        this.email = email;
    }

    public String returnUserData() {
        return "Username: " + this.username;
    }
}
