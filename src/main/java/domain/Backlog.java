package domain;

import java.util.ArrayList;
import java.util.List;

public class Backlog {
    List<Task> tasks;

    public Backlog() {
        tasks = new ArrayList<Task>();
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public List<Task> returnTasks() {
        return tasks;
    }

    public Task getTask(Task task) {
        return task;
    }
}
