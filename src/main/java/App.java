import adapter.EmailAdapter;
import adapter.GmailSender;
import adapter.Sender;
import domain.Project;
import domain.User;
import factory.CreateBoardFactory;
import observer.EmailObserver;
import observer.MessageObserver;
import observer.Observer;
import strategy.AzurePipelineStrategy;
import template.MessageTemplate;

public class App {
    public static void main(String[] args) throws Exception {
        User user = new User("Name", "Username", "Email");
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("Board", factory, new AzurePipelineStrategy());
        project.isDone = true;
        Sender sender = new GmailSender();
        EmailObserver adapter = new EmailAdapter(sender);
        Observer observer = new MessageObserver(adapter);

        project.notifyObservers();

        System.out.println("-------------------------------------------------------------------------");
        MessageTemplate email = new GmailSender();
        email.composeMessage(project.doneState, user);
    }
}
