package adapter;

public interface Sender {
    void sendMessage(String message);
}
