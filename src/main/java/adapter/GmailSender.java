package adapter;

import template.MessageTemplate;

public class GmailSender extends MessageTemplate implements Sender {

    @Override
    public void sendMessage(String message) {
        System.out.println("===Gmail===");
        System.out.println(message);
    }

    @Override
    public String addMessage() {
        return "Gmail sent";
    }
}
