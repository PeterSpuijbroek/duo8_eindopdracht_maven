package adapter;

import template.MessageTemplate;

public class OutlookSender extends MessageTemplate implements Sender {

    @Override
    public void sendMessage(String message) {
        System.out.println("===Outlook===");
        System.out.println(message);
    }

    @Override
    public String addMessage() {
        return "Outlook sent";
    }
}
