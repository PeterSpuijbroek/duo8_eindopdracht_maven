package adapter;


import observer.EmailObserver;

public class EmailAdapter implements EmailObserver {

    Sender sender;

    public EmailAdapter(Sender sender) {
        this.sender = sender;
    }

    @Override
    public void sendEmail(String message) {
        this.sender.sendMessage("Message: " + message);
    }

}
