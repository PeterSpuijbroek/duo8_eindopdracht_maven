package factory;

import java.util.List;

import domain.BoardType;
import domain.Task;
import domain.User;

public interface Board {
    BoardType getBoardType();
    String getBoardName();
    List<Task> getTaskList();
    List<User> getUserList();
}
