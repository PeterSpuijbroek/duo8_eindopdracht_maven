package factory;

import domain.BoardType;

public class CreateBoardFactory {
    public Board createBoard(String name, BoardType type) {
        if(type == BoardType.SCRUM) {
            return new ScrumBoard(name);
        }
        else if(type == BoardType.KANBAN) {
            return new KanbanBoard(name);
        }
        else {
            return null;
        }
    }
}
