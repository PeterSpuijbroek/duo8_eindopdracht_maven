package factory;

import java.util.ArrayList;
import java.util.List;

import domain.BoardType;
import domain.Task;
import domain.User;

public class ScrumBoard implements Board {

    String name;
    int sprintNr;
    BoardType boardType;
    List<Task> tasks;
    List<User> users;

    public ScrumBoard(String name) {
        this.name = name;
        sprintNr = 1;
        boardType = BoardType.SCRUM;
        tasks = new ArrayList<Task>();
        users = new ArrayList<User>();
    }

    @Override
    public BoardType getBoardType() {
        return boardType;
    }

    public int getSprintNr() {
        return sprintNr;
    }

    @Override
    public String getBoardName() {
        return name;
    }

    @Override
    public List<Task> getTaskList() {
        return tasks;
    }

    @Override
    public List<User> getUserList() {
        return users;
    }

}
