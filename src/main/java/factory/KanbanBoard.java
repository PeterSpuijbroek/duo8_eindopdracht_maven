package factory;

import java.util.ArrayList;
import java.util.List;

import domain.BoardType;
import domain.Task;
import domain.User;

public class KanbanBoard implements Board {

    String name;
    BoardType type;
    List<Task> tasks;
    List<User> users;

    public KanbanBoard(String name) {
        this.name = name;
        type = BoardType.KANBAN;
        tasks = new ArrayList<Task>();
        users = new ArrayList<User>();
    }

    @Override
    public BoardType getBoardType() {
        return type;
    }

    @Override
    public String getBoardName() {
        return name;
    }

    @Override
    public List<Task> getTaskList() {
        return tasks;
    }

    @Override
    public List<User> getUserList() {
        return users;
    }

}
