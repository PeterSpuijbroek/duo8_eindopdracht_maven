package template;

import domain.User;
import state.TaskState;

public abstract class MessageTemplate {
    public final void composeMessage(TaskState state, User user) {
        String taskState = getTaskState(state);
        String userData = getUserData(user);
        String message = addMessage();

        System.out.println("===========================");
        System.out.println("ANOTHER TASK IS DONE");
        System.out.println(taskState);
        System.out.println(userData);
        System.out.println(message);
        System.out.println("===========================");
    }

    final String getTaskState(TaskState state) {
        return state.toString();
    }

    final String getUserData(User user) {
        return user.returnUserData();
    }

    public abstract String addMessage();
}
