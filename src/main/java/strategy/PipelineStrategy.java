package strategy;

public interface PipelineStrategy {
    String run();
}
