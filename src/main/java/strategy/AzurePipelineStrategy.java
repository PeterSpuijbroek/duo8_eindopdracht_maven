package strategy;

public class AzurePipelineStrategy implements PipelineStrategy {
    @Override
    public String run() {
        return "Running on Azure...";
    }
}
