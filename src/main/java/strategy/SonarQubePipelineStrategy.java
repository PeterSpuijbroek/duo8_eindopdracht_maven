package strategy;

public class SonarQubePipelineStrategy implements PipelineStrategy {
    @Override
    public String run() {
        return "Running on SonarQube...";
    }
}
