package observer;

public interface EmailObserver {
    void sendEmail(String message);
}
