package observer;

import state.TaskState;

public interface Observer {
    void update(TaskState state);
}
