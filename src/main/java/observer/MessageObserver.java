package observer;

import state.TaskState;

public class MessageObserver implements Observer {

    EmailObserver emailObserver;

    public MessageObserver(EmailObserver emailObserver) {
        this.emailObserver = emailObserver;
    }

    @Override
    public void update(TaskState state) {
        String message = "Task is done";
        this.emailObserver.sendEmail(message);
    }
}
