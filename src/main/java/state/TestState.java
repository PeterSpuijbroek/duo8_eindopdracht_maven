package state;

import domain.Project;

public class TestState implements TaskState {
    Project project;

    public TestState(Project project) {
        this.project = project;
    }

    @Override
    public void makeToDoState() {
        System.out.println("Can't change state from Test-State to To-Do-State");
    }

    @Override
    public void makeInProgressState() {
        System.out.println("Can't change state from Test-State to In-Progress-State");
    }

    @Override
    public void makeTestState() {
        System.out.println("Already in Test-State");
    }

    @Override
    public void makeCodeReviewState() {
        System.out.println("Can't change state from Test-State to Code-Review-State");
    }

    @Override
    public void makeDoneState() {
        project.isDone = true;
        project.setState(project.doneState);
    }

    @Override
    public String toString() {
        return "TestState";
    }

}
