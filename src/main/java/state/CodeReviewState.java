package state;

import domain.Project;

public class CodeReviewState implements TaskState {
    Project project;

    public CodeReviewState(Project project) {
        this.project = project;
    }

    @Override
    public void makeToDoState() {
        System.out.println("Can't change state from Code-Review-State to To-Do-State");
    }

    @Override
    public void makeInProgressState() {
        project.setState(project.inProgressState);
    }

    @Override
    public void makeTestState() {
        project.setState(project.testState);
    }

    @Override
    public void makeCodeReviewState() {
        System.out.println("Already in Code-Review-State");
    }

    @Override
    public void makeDoneState() {
        System.out.println("Can't change state from Code-Review-State to Done-State");
    }

    @Override
    public String toString() {
        return "CodeReviewState";
    }

}
