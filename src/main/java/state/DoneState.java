package state;

import domain.Project;

public class DoneState implements TaskState {
    Project project;

    public DoneState(Project project) {
        this.project = project;
    }

    @Override
    public void makeToDoState() {
        System.out.println("Task is already finished");
    }

    @Override
    public void makeInProgressState() {
        System.out.println("Task is already finished");
    }

    @Override
    public void makeTestState() {
        System.out.println("Task is already finished");
    }

    @Override
    public void makeCodeReviewState() {
        System.out.println("Task is already finished");
    }

    @Override
    public void makeDoneState() {
        System.out.println("Task is already finished");
    }

    @Override
    public String toString() {
        return "DoneState";
    }
}
