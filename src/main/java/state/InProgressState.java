package state;

import domain.Project;

public class InProgressState implements TaskState {
    Project project;

    public InProgressState(Project project) {
        this.project = project;
    }

    @Override
    public void makeToDoState() {
        System.out.println("Can't change state from In-Progress-State to To-Do-State");
    }

    @Override
    public void makeInProgressState() {
        System.out.println("Already in In-Progress-State");
    }

    @Override
    public void makeTestState() {
        System.out.println("Can't change state from In-Progress-State to Test-State");
    }

    @Override
    public void makeCodeReviewState() {
        project.setState(project.codeReviewState);
    }

    @Override
    public void makeDoneState() {
        System.out.println("Can't change state from In-Progress-State to Done-State");
    }

    @Override
    public String toString() {
        return "InProgressState";
    }

}