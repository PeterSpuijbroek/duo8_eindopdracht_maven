package state;

import domain.Project;

public class ToDoState implements TaskState {
    Project project;

    public ToDoState(Project project) {
        this.project = project;
    }

    @Override
    public void makeToDoState() {
        System.out.println("Already in To-Do-State");
    }

    @Override
    public void makeInProgressState() {
        project.setState(project.getInProgressState());
    }

    @Override
    public void makeTestState() {
        System.out.println("Can't change state from To-Do-State to Test-State");
    }

    @Override
    public void makeCodeReviewState() {
        System.out.println("Can't change state from To-Do-State to Code-Review-State");
    }

    @Override
    public void makeDoneState() {
        System.out.println("Can't change state from To-Do-State to Done-State");
    }

    @Override
    public String toString() {
        return "ToDoState";
    }

}
