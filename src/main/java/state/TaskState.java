package state;

public interface TaskState {
    public void makeToDoState();

    public void makeInProgressState();

    public void makeTestState();

    public void makeCodeReviewState();

    public void makeDoneState();

    public String toString();
}
