import static org.junit.jupiter.api.Assertions.assertEquals;

import domain.Project;
import factory.CreateBoardFactory;
import org.junit.jupiter.api.Test;
import state.TaskState;
import strategy.AzurePipelineStrategy;

public class Task_Test {
    @Test
    public void Test_ToDoStateSetsToInProgressState() {
        Project project = new Project("test", null, new AzurePipelineStrategy());
        TaskState state = project.getState();

        state.makeInProgressState();

        assertEquals(project.getState(), project.getInProgressState());
    }

    @Test
    public void Test_InProgressStateSetsToInReviewState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());
        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();

        assertEquals(project.getState(), project.getCodeReviewState());
    }

    @Test
    public void Test_InReviewStateSetsToInProgressState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());
        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState codeReview = project.getState();
        codeReview.makeInProgressState();

        assertEquals(project.getState(), project.getInProgressState());
    }

    @Test
    public void Test_InReviewStateSetsToInTestState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());
        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState codeReview = project.getState();
        codeReview.makeInProgressState();
        codeReview.makeTestState();

        assertEquals(project.getState(), project.getTestState());
    }

    @Test
    public void Test_InTestStateSetsToDoneState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());

        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState codeReview = project.getState();
        codeReview.makeTestState();
        TaskState inTest = project.getState();
        state.makeTestState();
        inTest.makeDoneState();

        assertEquals(project.doneState, project.getState());
    }

    @Test
    public void Test_InProgressStateKeepsInProgressState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());

        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeTestState();

        assertEquals(project.inProgressState, project.getState());
    }

    @Test
    public void Test_InReviewStateKeepsInReviewState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());

        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState inReview = project.getState();
        inReview.makeDoneState();

        assertEquals(project.codeReviewState, project.getState());
    }

    @Test
    public void Test_InTestStateKeepsInTestState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());

        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState codeReview = project.getState();
        codeReview.makeTestState();
        TaskState inTest = project.getState();
        state.makeTestState();
        inTest.makeCodeReviewState();

        assertEquals(project.testState, project.getState());
    }

    @Test
    public void Test_InDoneStateKeepsInDoneState() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());

        TaskState state = project.getState();

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState inReview = project.getState();
        inReview.makeTestState();
        TaskState inTest = project.getState();
        inTest.makeDoneState();
        TaskState doneState = project.getState();
        doneState.makeTestState();

        assertEquals(project.doneState, project.getState());
    }

}
