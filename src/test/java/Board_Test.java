

import domain.BoardType;
import factory.Board;
import factory.CreateBoardFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Board_Test {
    @Test
    public void Test_CreateBoardFactoryCreatesNewScrumBoard() {
        CreateBoardFactory factory = new CreateBoardFactory();

        Board result = factory.createBoard("scrum", BoardType.SCRUM);

        assertEquals("scrum", result.getBoardName());
        assertEquals(BoardType.SCRUM, result.getBoardType());
    }

    @Test
    public void Test_CreateBoardFactoryCreatesNewKanbanBoard() {
        CreateBoardFactory factory = new CreateBoardFactory();

        Board result = factory.createBoard("kanban", BoardType.KANBAN);

        assertEquals("kanban", result.getBoardName());
        assertEquals(BoardType.KANBAN, result.getBoardType());
    }
}
