

import adapter.EmailAdapter;
import adapter.GmailSender;
import adapter.Sender;
import domain.Project;
import factory.CreateBoardFactory;
import observer.EmailObserver;
import observer.MessageObserver;
import observer.Observer;
import org.junit.jupiter.api.Test;
import state.TaskState;
import strategy.AzurePipelineStrategy;
import strategy.SonarQubePipelineStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Project_Test {
    @Test
    public void Test_NotifyObserversDoesNotSendMessageWhenTaskStateIsNotDone() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());
        TaskState state = project.getState();
        Sender gmail = new GmailSender();
        EmailObserver emailAdapter = new EmailAdapter(gmail);
        Observer observer = new MessageObserver(emailAdapter);
        project.registerObserver(observer);

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState codeReview = project.getState();
        codeReview.makeTestState();
        boolean done = project.isDone;

        assertEquals(false, done);
    }

    @Test
    public void Test_NotifyObserversDoesSendMessageWhenTaskStateIsDone() {
        CreateBoardFactory factory = new CreateBoardFactory();
        Project project = new Project("test", factory, new AzurePipelineStrategy());
        TaskState state = project.getState();
        Sender gmail = new GmailSender();
        EmailObserver emailAdapter = new EmailAdapter(gmail);
        Observer observer = new MessageObserver(emailAdapter);
        project.registerObserver(observer);

        state.makeInProgressState();
        TaskState inProgress = project.getState();
        inProgress.makeCodeReviewState();
        TaskState codeReview = project.getState();
        codeReview.makeTestState();
        TaskState test = project.getTestState();
        test.makeDoneState();
        boolean done = project.isDone;

        assertEquals(true, done);
    }

    @Test
    public void Test_AzurePipelineStrategyReturnsAzureString() {
        Project project = new Project("test", null, new AzurePipelineStrategy());

        String result = project.runPipeline();

        assertEquals("Running on Azure...", result);
    }

    @Test
    public void Test_SonarQubePipelineStrategyReturnsAzureString() {
        Project project = new Project("test", null, new SonarQubePipelineStrategy());

        String result = project.runPipeline();

        assertEquals("Running on SonarQube...", result);
    }
}
